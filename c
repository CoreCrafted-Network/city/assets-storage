INPUTSTR=$1
MESSAGE=${INPUTSTR:="Update"}
echo ====== [Pull] ======
git pull
echo ====== [Stage] ======
git add --all
echo ====== [Status] ======
git status
echo ====== [Commit/Push] ======
git commit -m $MESSAGE
git push